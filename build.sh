#!/bin/bash

# put all .jar files into directory jars/

javac -cp "jars/*" Main.java

if [ $? -eq 0 ]; then
	echo "comp ok"
	java -cp ".:jars/*" Main
else 
  	echo "comp ko"
fi
